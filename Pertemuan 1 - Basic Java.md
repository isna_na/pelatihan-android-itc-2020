# Basic Java
## Pendahuluan
Java adalah bahasa pemrograman yang dibuat oleh James Gosling. Java dirancang untuk tujuan umum (general-purpose) dan sepenuhnya menganut paradigma OOP (Object Oriented Programming).

**Kegunaan Java :**
1. Mobile applications (specially Android apps)
2. Desktop applications
3. Web applications
4. Web servers and application servers
5. Games
6. Database connection

**Keunggulan Java :**
1. Java Dapat Dijalankan di Platform yang Berbeda(Windows, Mac, Rasberry Pi, DLL).
2. Open-Source dan Gratis.
3. Comumunity Support yang Luas(Lebih dari 10 Juta Devoloper).
4. Java Merupakan Bahasa Program yang Berbasis Object.
5. Syntax yang Mirip dengan C++ atau C#.

## Struktur dan Syntax pada Java
Setiap bahasa pemrograman memiliki struktur dan aturan penulisan sintaks yang berbeda-beda. Coba perhatikan program berikut:
```java
package com.ITC;
class Program {
    public static void main(String args[]){
        System.out.println("Hello World");
    }
}
```
Dari contoh diatas dapat kita lihat bahwa berbeda dengan C++, pada java banyak struktur struktur yang sedikit asing terutama untuk para developer yang menggunakan bahasa pemrograman seperti C++, Python, dll.

Struktur program Java secara umum dibagi menjadi 4 bagian:
1. **Deklarasi Package**
Package merupakan sebuah folder yang berisi sekumpulan program Java. Tujuan dari penggunaan package adalah agar file dapat tersusun dengan rapih terlebih pada saat membuat program atau aplikasi besar.
2. **Impor Library**
Library merupakan sekumpulan class dan fungsi yang bisa kita gunakan dalam membuat program. Jika kita memerlukan fungsi dari class lain maka kita dapat mengimpor library ke dalam file java kita.
3. **Bagian Class**
Java merupakan bahasa pemrograman yang menggunakan paradigma OOP (Object Oriented Programming). Setiap program harus dibungkus di dalam class agar nanti bisa dibuat menjadi objek. Nama Class harus sama seperti nama file java.
4. **Method Main**
Method main() atau fungsi main() merupakan blok program yang akan dieksekusi pertama kali sama seperti int main() pada C++. Method main merupakan entri point dari program.
Contoh Penulisan : 
    ```java
    package com.ITC; // Deklarasi package
    import java.io.File; // Impor library
    class Program { // Class
        public static void main(String args[]){ // Method
            System.out.println("Hello World");
        }
    }
    ```

## Java OOP

### Inheritance

### Encapsulation (Setter dan Getter)
Setter dan Getter pada java kita gunakan apabila kita ingin mengases suatu variable private atau protected(Jika bukan inhretance) dari suatu class.

Contoh : 
```java
private String data;
public String getData(){//Fungsi Getter
    return this.data;
}

public void setData(String data) {//Fungsi Setter
    this.data = data;
}
```
> Note : 
> Pastikan modifier fungsi getter dan setter adalah public agar fungsi dapat diakses oleh class lain.
> Tipe data variable dan fungsi getter harus sama dan pada setter fungsi bertipe void.

### OverLoading & Overriding

### Interface

link referensi lain untuk belajar :
- [Java Basic](https://www.youtube.com/playlist?list=PLZS-MHyEIRo51w0Hmqi0C8h2KWNzDfo6F)
- [OOP Java](https://www.youtube.com/playlist?list=PLZS-MHyEIRo6V4_vk1s1NcM2HoW5KFG7i)
