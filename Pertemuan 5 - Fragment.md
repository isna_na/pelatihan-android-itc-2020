# Fragment
### Pendahuluan
Fragment adalah sebuah reuseable class yang mengimplement beberapa fitur sebuah Activity. Fragment biasanya dibuat sebagai bagian dari suatu antarmuka. Sebuah fragment harus berada di dalam sebuah activity, mereka tidak dapat berjalan sendiri tanpa adanya activity tempat mereka menempel.

Berikut ini beberapa hal yang perlu dipahami tentang fragment:
1.	Sebuah Fragment merupakan kombinasi sebuah layout XML dan kelas java yang mirip dengan sebuah Activity.
2.	Dengan menggunakan support library, fragment dapat mendukung hampir semua versi Android.
3.	Fragment dapat dipakai berulang kali didalam activity.
4.	Fragment merupakan komponen utuh yang memiliki view, event, dan logic (meskipun tetap membutuhkan sebuah fragment agar dapat bekerja).

Ada banyak kasus yang dapat diselesaikan menggunakan fragment, namun yang paling umum adalah:
- Penggunaan Komponen View dan Logic Berulang Kali - Fragment dapat dipakai untuk menampilkan data atau melakukan event tertentu dibeberapa activity berbeda.
- Dukungan Untuk Tablet - Dibeberapa aplikasi, versi tablet dari sebuah activity memberikan layout yang berbeda dari versi handphone yang juga berbeda dari versi TV. Fragment memungkinkan activity untuk menggunakan fragment dalam membuat antarmuka sesuai dengan perangkat yang membukanya.
- Orientasi Layar - Seringkali dalam aplikasi, versi portrait dan landscape sebuah aplikasi memiliki layout yang berbeda. Fragment memungkinkan kedua orientasi tersebut untuk menggunakan tampilan yang berbeda menggunakan elemen yang sama.

### Fragment Lifecycle
Siklus proses pada Fragment sangat mirip dengan siklus proses pada suatu Activity. Seperti Activity, Fragment bisa ada dalam tiga status siklus :
1. Resumed
> Fragment dapat terlihat ketika activity sedang dijalankan.
2. Paused
> Terjadi ketika terdapat Activity lain yang berada di depan Activity tempat Fragment ini berada. Dimana Activity baru yang berada di depan tidak menutupi seluruh layar.
3. Stopped
> Terjadi ketika Fragment sudah tidak terlihat lagi di layar. Dapat terjadi karena Activity tempat Fragment itu berada telah berhenti atau sudah terhapus. Walau sudah terhapus, Fragment ini masih tetap hidup dengan semua informasinya. 

Skema di bawah ini menunjukkan siklus proses pada Fragment lebih lengkapnya.

![Fragment Lifecycle](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/fragment_lifecycle.png)

### Membuat Fragment
Agar anda lebih memahami materi Fragment, akan ada sebuah project yang akan mencontohkan pembuatan sebuah Fragment dengan jenis Bottom Navigation.
1. Buatlah project baru di Android Studio dengan nama FragmentKu.
2. Buatlah _**Android Resource Directory**_ baru dengan nama _**menu**_. Caranya adalah klik kanan pada bagian _**res**_ → New → Android Resource Directory. Setelah tampil dialognya, ubah _**Resource type**_ menjadi _**menu**_ dan _**Directory Name**_ menjadi _**menu**_. Lalu klik _**OK**_ untuk menyelesaikan proses.
3. Buatlah file xml baru dengan nama _**menu_navigation.xml**_ dengan cara klik kanan pada directory _**menu**_ -> _**New**_ -> _**Menu Resource File**_ -> Lalu isikan _**menu_navigation**_ pada bagian _**File Name**_ -> Klik _**OK**_ untuk menyelesaikan proses.
4. Pada _**menu_navigation.xml**_, sesuaikan kodingan yang telah ada menjadi,
    ```java
    <?xml version="1.0" encoding="utf-8"?>
    <menu xmlns:android="http://schemas.android.com/apk/res/android">
        <item
            android:id="@+id/menu_home"
            android:title="Home"
            android:icon="@drawable/icon_home" />
        <item
            android:id="@+id/menu_profile"
            android:title="Profile"
            android:icon="@drawable/icon_person"/>
    </menu>
    ```
5. Setelah itu, sesuaikan juga pada file _**activity_main.xml**_ menjadi,
    ```java
    <?xml version="1.0" encoding="utf-8"?>
    <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".MainActivity"
        android:orientation="vertical">

        <FrameLayout
            android:id="@+id/frame_container"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_weight="1">
        </FrameLayout>

        <android.support.design.widget.BottomNavigationView
            android:background="#CCD4D8"
            android:id="@+id/bottom_nav"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:menu="@menu/menu_navigation"/>

    </LinearLayout>
    ```
6. Setelah selesai mengedit file _**activity_main.xml**_, kita akan membuat sebuah Fragment dengan nama HomeFragment. Caranya adalah klik kanan pada package utama pada proyek aplikasi Anda → New → Fragment → Fragment (Blank).
7. Setelah tampil dialog Fragment, isikan _**HomeFragment**_ pada _**Fragment Name**_ dan uncheck untuk kedua pilihan (include fragment factory methods dan include interface callbacks). Lalu klik _**Finish**_ untuk selesai.
8. Setelah membuat _**HomeFragment**_, sesuaikan kodingan yang ada pada _**fragment_home.xml**_ menjadi seperti berikut ini,
    ```java
    <?xml version="1.0" encoding="utf-8"?>
    <FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".HomeFragment">
        
        <TextView
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:text="INI FRAGMENT HOME" />

    </FrameLayout>
    ```
9. Lakukan hal yang sama untuk membuat Fragment baru dengan nama _**ProfileFragment**_ dan sesuaikan kodingan yang ada pada _**fragment_profile.xml**_ menjadi seperti berikut ini,
    ```java
    <?xml version="1.0" encoding="utf-8"?>
    <FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".ProfileFragment">

        <TextView
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:text="INI FRAGMENT PROFILE" />

    </FrameLayout>
    ```
10. Pada bagian _**HomeFragment.java**_ dan _**ProfileFragment.java**_ memiliki isi sebagai berikut,
    - ``` HomeFragment.java ```
        ```java
        public class HomeFragment extends Fragment {
            public HomeFragment() {
                // Required empty public constructor
            }

            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                    Bundle savedInstanceState) {
                // Inflate the layout for this fragment
                return inflater.inflate(R.layout.fragment_home, container, false);
            }

            @Override
            public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
                super.onViewCreated(view, savedInstanceState);
                // Tempat untuk ngoding. Fungsinya sama seperti method onCreate di Activity
            }
        }

        ```
    - ``` ProfileFragment.java ```
        ```java
        public class ProfileFragment extends Fragment {
            public ProfileFragment() {
                // Required empty public constructor
            }

            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                    Bundle savedInstanceState) {
                // Inflate the layout for this fragment
                return inflater.inflate(R.layout.fragment_profile, container, false);
            }

            @Override
            public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
                super.onViewCreated(view, savedInstanceState);
                // Tempat untuk ngoding. Fungsinya sama seperti method onCreate di Activity
            }
        }
        ```
11. Selanjutnya, pada _**MainActivity**_ kita sambungkan projek kita dengan Fragment dengan melakukan penyesuaian kodingan seperti berikut ini,
    ```java
    public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

        private BottomNavigationView btnNav;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            btnNav = findViewById(R.id.bottom_nav);

            btnNav.setOnNavigationItemSelectedListener(this);
            loadFragment(new HomeFragment());
        }

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            switch (menuItem.getItemId())
            {
                case R.id.menu_home:
                    loadFragment(new HomeFragment());
                    break;

                case R.id.menu_profile:
                    loadFragment(new ProfileFragment());
                    break;
            }
            return true;
        }
        // Method ini digunakan untuk memilih fragment yang akan dijalankan.

        private boolean loadFragment(Fragment selectedFragment) {
            if (selectedFragment != null)
            {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frame_container,selectedFragment)
                        .commit();
                return true;
            }
            else
            {
                return false;
            }
        }
        // Method ini digunakan untuk menempelkan fragment pada activity.
    }
    ```
12. Setelah selesai semua prosesnya, hasilnya akan jadi seperti berikut ini,
- Berikut adalah tampilan ketika Fragment Home di klik.

    ![Fragment Home](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/fragment_home.jpeg)
- Berikut adalah tampilan ketika Fragment Profile di klik.

    ![Fragment Profile](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/fragment_profile.jpeg)

Anda dapat membaca lebih lanjut mengenai Fragment pada tautan berikut:
- [Fragment](https://developer.android.com/guide/components/fragments)