# Project Akhir
Halo teman-teman, gimana pelatihan android dari ITC kali ini. Pada paham semua kan materi-materinya. Pasti paham dong... Jadi gini, di pertemuan terakhir kali ini kita akan membuat project akhir dengan ketentuan-ketentuan seperti di bawah ini. Oh iya, sama proses pembuatan project ini berlangsung selama seminggu ya... Jangan telat OK, SEMANGATTT!  

### Ketentuan Project
1. Menggunakan semua materi yang diajarkan mulai dari pertemuan pertama (Basic Java) hingga pertemuan kedelapan (API).
2. Untuk jumlah activity yang ada di dalam project minimal menggunakan **5 halaman**, yang terdiri dari **3 halaman wajib** (Home, Detail, Profile) dan **2 halaman bebas** sesuai dengan kreativitas masing-masing.
3. Untuk desain layout yang digunakan di dalam project harus dibuat menarik, layak, pemilihan warna yang sesuai dan bagus.  
4. Untuk SDK yang dipilih pada project minimal menggunakan Lollipop.
5. Untuk penggunaan API dapat memilih dari 3 API yang telah disediakan, yaitu :
    - SportDB : https://www.thesportsdb.com/api.php
    - GithubAPI : https://api.github.com/
    - ThemealDB : https://www.themealdb.com/api.php
6. Untuk penggunaan GithubAPI, ada beberapa endpoint yang dapat digunakan untuk mengambil data seperti berikut ini,
    - Search : https://api.github.com/search/users?q={username}
    - Detail user : https://api.github.com/users/{username}
    - List Follower : https://api.github.com/users/{username}/followers
    - List Following : https://api.github.com/users/{username}/following
7. Tidak diperbolehkan melakuka plagiasi.
8. Project dilakukan secara individu.
9. Penilaian project dilakukan dengan cara wawancara secara terjadwal
    - Wawancara dilakukan selama 2 hari
    - Wawancara dibagi menjadi 2 kelompok
    - Wawancara dilakukan satu persatu
    - 1 peserta diwawancarai oleh 2 orang
    - Wawancara menggunakan platform Google Meet
    - Jadwal wawancara akan diberikan 1 hari setelah deadline pengumpulan project

### Kriteria Project

#### 1. Halaman Utama

**Syarat :**
- Menampilkan Informasi dalam format List dengan jumlah minimal 10 Item yang berbeda.
- Memunculkan halaman detail ketika salah satu item ditekan.
- Menambahkan 2 fragment bottom navigation, yaitu Home dan Profile.

#### 2. Halaman Detail

**Syarat :**
- Menampilkan gambar dan informasi yang relevan pada halaman detail.
- Menambahkan menu back pada action bar.


#### 3. Halaman Profile

**Syarat :**
- Menampilkan foto diri, nama, dan email.
- Menambahkan 2 fragment bottom navigation, yaitu Home dan Profile.

### Prototype Project

![Prototype](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/prototype.png)