# View and View Group
### Pendahuluan
Pada dasarnya semua elemen antar pengguna di aplikasi android dibangun menggunakan dua buah komponen inti yaitu View dan ViewGroup.

### View
Sebuah View adalah obyek yang menggambar komponen tampilan ke layar yangmana pengguna dapat melihat dan berinteraksi langsung.

**Komponen turunan dari View :**
1. TextView, komponen yang berguna untuk menampilkan teks ke layar.
2. Button, komponen yang membuat pengguna dapat berinteraksi dengan cara ditekan untuk melakukan sesuatu.
3. ImageView, Komponen untuk menampilkan gambar.
4. RadioButton, komponen yang memungkinkan pengguna dapat memilih satu pilihan dari berbagai pilihan yang disediakan.
5. Checkbox, komponen yang memungkinkan pengguna dapat memilih lebih dari satu dari pilihan yang ada.
6. EditText, Komponen yang berguna untuk memasukan input text(String) ke program.

**Atribut View**

Android Studio mendukung berbagai atribut XML yang memungkinkan developer untuk mengatur view dan memperindah view.

1. ```id="@+id/itemText"``` : Menambahkan ID pada attribut.
2. ```android:layout_width="match_parent"``` : Mengatur lebar view.
3. ```android:layout_height="wrap_content"``` : Mengatur tinggi view.
4. ```android:layout_gravity="center"``` : Mengatur align view terhadap layout.
5. ```android:gravity="center"``` : Mengatur align isi view terhadap view.
6. ```android:padding="20dp"``` : Mengatur padding view.
7. ```android:textSize="20sp"``` : Mengatur besar text pada view.
8. ```android:textColor="#000000"``` : Mengubah warna text pada view.
9. ```android:text="Ini Text"``` : Mengatur isi text pada view.
10. dan masih banyak lagi.

### View Group
Sebuah obyek yang mewadahi obyek-obyek View dan ViewGroup itu sendiri sehingga membentuk satu kesatuan tampilan aplikasi yang utuh. 

**ViewGroup lebih seperti :**
1. Linear Layout
> ![Contoh ViewGroup](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/gambar.png)
>
> Jenis tampilan yang menyejajarkan semua komponen view yang berada di dalamnya dalam satu arah, secara vertikal maupun horizontal. Linearlayout memiliki atribut weight untuk masing-masing child view yang berguna untuk menentukan porsi ukuran view dalam sebuah  ruang (*space*) yang tersedia.
>
> Contoh Kodingan Orientasi Vertikal
>>```
>><?xml version="1.0" encoding="utf-8"?>
>><LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
>>    android:layout_width="wrap_content"
>>    android:layout_height="wrap_content"
>>    android:orientation="vertical" >
>>    <Button
>>        android:layout_width="100dp"
>>        android:layout_height="wrap_content"
>>        android:text="Button 1"
>>        android:background="@color/colorAccent"
>>        android:layout_margin="2dp"
>>        android:textColor="#fff"
>>        android:textStyle="bold"/>
>>    <Button
>>        android:layout_width="100dp"
>>        android:layout_height="wrap_content"
>>        android:text="Button 2"
>>        android:background="@color/colorAccent"
>>        android:layout_margin="2dp"
>>        android:textColor="#fff"
>>        android:textStyle="bold"/>
>>    <Button
>>        android:layout_width="100dp"
>>        android:layout_height="wrap_content"
>>        android:text="Button 3"
>>        android:background="@color/colorAccent"
>>        android:layout_margin="2dp"
>>        android:textColor="#fff"
>>        android:textStyle="bold"/>
>></LinearLayout>
>> ```
> Contoh Kodingan Orientasi Horizontal
>>```
>><?xml version="1.0" encoding="utf-8"?>
>><LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
>>    android:layout_width="wrap_content"
>>    android:layout_height="wrap_content"
>>    android:orientation="horizontal" >
>>    <Button
>>        android:layout_width="100dp"
>>        android:layout_height="wrap_content"
>>        android:text="Button 1"
>>        android:background="@color/colorAccent"
>>        android:layout_margin="2dp"
>>        android:textColor="#fff"
>>        android:textStyle="bold"/>
>>    <Button
>>        android:layout_width="100dp"
>>        android:layout_height="wrap_content"
>>        android:text="Button 2"
>>        android:background="@color/colorAccent"
>>        android:layout_margin="2dp"
>>        android:textColor="#fff"
>>        android:textStyle="bold"/>
>>    <Button
>>        android:layout_width="100dp"
>>        android:layout_height="wrap_content"
>>        android:text="Button 3"
>>        android:background="@color/colorAccent"
>>        android:layout_margin="2dp"
>>        android:textColor="#fff"
>>        android:textStyle="bold" />
>></LinearLayout>
>> ```
> Anda dapat membaca lebih lanjut mengenai LinearLayout pada tautan berikut:
> - [LinearLayout](https://developer.android.com/guide/topics/ui/layout/linear.html)
2. Relative Layout
> ![Contoh ViewGroup](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/gambar3.png)
>
> Layout yang lebih fleksibel daripada LinearLayout. Hal ini dikarenakan posisi dari masing-masing komponen di dalamnya dapat mengacu secara relatif pada komponen yang lainnya dan juga dapat mengacu secara relatif ke batas layar.
>
> Contoh Kodingan
>>```
>><RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
>>    android:layout_width="match_parent"
>>    android:layout_height="match_parent"
>>    android:background="#ef0000" >
>>    <EditText
>>        android:id="@+id/name"
>>        android:layout_width="200dp"
>>        android:layout_height="wrap_content"
>>        android:background="#f8eff8f3"
>>        android:hint="Nama "
>>        android:padding="16dp" />
>>    <EditText
>>        android:id="@+id/email"
>>        android:layout_width="180dp"
>>        android:layout_height="wrap_content"
>>        android:layout_marginLeft="5dp"
>>        android:layout_toRightOf="@+id/name"
>>        android:background="#f8eff8f3"
>>        android:hint="Alamat "
>>        android:padding="16dp" />
>>    <EditText
>>        android:id="@+id/subject"
>>        android:layout_width="match_parent"
>>        android:layout_height="wrap_content"
>>        android:layout_below="@+id/name"
>>        android:layout_marginTop="16dp"
>>        android:background="#f8eff8f3"
>>        android:hint="Subjek"
>>        android:padding="16dp" />
>>    <EditText
>>        android:id="@+id/message"
>>        android:layout_width="match_parent"
>>        android:layout_height="200dp"
>>        android:layout_below="@+id/subject"
>>        android:layout_marginTop="16dp"
>>        android:background="#f8eff8f3"
>>        android:gravity="top"
>>        android:hint="Pesan"
>>        android:padding="16dp" />
>>    <Button
>>        android:id="@+id/submit"
>>        android:layout_width="wrap_content"
>>        android:layout_height="wrap_content"
>>        android:layout_below="@+id/message"
>>        android:layout_centerHorizontal="true"
>>        android:layout_marginTop="16dp"
>>        android:padding="16dp"
>>        android:text="KIRIM" />
>></RelativeLayout>
>>```
> Anda dapat membaca lebih lanjut mengenai RelativeLayout pada tautan berikut:
> - [RelativeLayout](https://developer.android.com/guide/topics/ui/layout/relative.html)
3. Scroll View
> ![Contoh ViewGroup](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/gambar2.png)
>
> Jenis layout yang memungkinkan komponen di dalamnya digeser (scroll) secara vertikal maupun horizontal. Komponen di dalam ScrollView hanya diperbolehkan memiliki 1 parent utama dari linearlayout, relativelayout, framelayout, atau tablelayout.
>
> Contoh Kodingan
>>```
>><?xml version="1.0" encoding="utf-8"?>
>><ScrollView
>>  android:layout_width="match_parent"
>>  android:layout_height="match_parent">
>>    <LinearLayout
>>        android:layout_width="match_parent"
>>        android:layout_height="wrap_content"
>>        android:orientation="vertical" >   
>>          <TextView
>>              android:layout_width="wrap_content"
>>              android:layout_height="match_parent"
>>              android:text="Hello" >
>>          </TextView>
>>          <TextView
>>              android:layout_width="wrap_content"
>>              android:layout_height="match_parent"
>>              android:text="Hello" >
>>          </TextView>
>>          <TextView
>>              android:layout_width="wrap_content"
>>              android:layout_height="match_parent"
>>              android:text="Hello" >
>>          </TextView>
>>          <TextView
>>              android:layout_width="wrap_content"
>>              android:layout_height="match_parent"
>>              android:text="Hello" >
>>          </TextView>
>>          <TextView
>>              android:layout_width="wrap_content"
>>              android:layout_height="match_parent"
>>              android:text="Hello" >
>>          </TextView>
>>          <TextView
>>              android:layout_width="wrap_content"
>>              android:layout_height="match_parent"
>>              android:text="Hello" >
>>          </TextView>
>>          <TextView
>>              android:layout_width="wrap_content"
>>              android:layout_height="match_parent"
>>              android:text="Hello" >
>>          </TextView>
>>          <TextView
>>              android:layout_width="wrap_content"
>>              android:layout_height="match_parent"
>>              android:text="Hello" >
>>          </TextView>
>>          <Button
>>              android:layout_width="wrap_content"
>>              android:layout_height="wrap_content"
>>              android:text="View" >
>>          </Button>
>>    </LinearLayout>
>></ScrollView>
>>```
> Anda dapat membaca lebih lanjut mengenai ScrollView pada tautan berikut:
> - [ScrollView](https://developer.android.com/reference/android/widget/ScrollView.html)

