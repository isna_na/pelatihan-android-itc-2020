# API
### Pendahuluan
Pada pertemuan-pertemuan sebelumnya, kita sudah membahas tentang apa itu RecyclerView dan bagaimana cara memakainya. Disana kita membuat Dummy Data untuk ditampilkan kedalam RecyclerView kita. Pada pertemuan kali ini, kita akan menggunakan data dalam bentuk yang sudah jadi dan siap diolah di dalam project kita. Untuk mengolah dan mengelola data tersebut, kita membutuhkan yang namanya API. 

### Intergrasi API
Agar anda dapat lebih memahami tentang materi API ini, kita masuk ke dalam project yang akan mencontohkan penggunaan API secara langsung.
1. Buatlah project baru di Android Studio dengan nama CoronaAPI.
2. Setelah project dibuat, tambahkan beberapa dependensi yang akan kita gunakan untuk project kita kali ini. Dependensi akan ditambahkan dalam file `build.gradle (module: app)` lalu pada bagian dependencies seperti ini
    ```java
    implementation 'com.squareup.retrofit2:retrofit:2.9.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
    ```
    Setelah itu tambahkan compileOption pada bagian android seperti ini,
    ```java
    android{
        ...

        compileOptions {
            sourceCompatibility JavaVersion.VERSION_1_8
            targetCompatibility JavaVersion.VERSION_1_8
        }
    }
    ``` 
3. Dikarenakan kita akan menggunakan data jadi yang berada di internet, kita perlu memberikan akses pada project kita agar data dapat terambil dengan lancar. Caranya adalah dengan menambahkan sebaris kode di dalam sebuah file `AndroidManifest.xml` di dalam package _**manifest**_ seperti berikut ini,
    ```java
    <manifest xmlns:android="http://schemas.android.com/apk/res/android"
        package="com.example.coronaapi">

        <uses-permission android:name="android.permission.INTERNET" />

        ...
    </manifest>
    ```
4. Selanjutnya kita akan membutuhkan sebuah _**Plugin**_ yang bernama _**RoboPOJOGenerator**_. Caranya adalah pilih _**File**_ -> _**Settings**_ -> _**Plugins**_ -> _**Marketplace**_ -> lalu cari _**RoboPOJOGenerator**_. Setelah itu, install dan restart IDE.

![RoboPOJO](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/robopojo.png)

5. Setelah selesai menginstall _**RoboPOJOGenerator**_, mari kita mulai masuk ke dalam API-nya. Buatlah dua buah package di dalam package utama dengan nama _**model**_ dan _**service**_. Caranya adalah dengan melakukan klik kanan pada package utama pada project aplikasi Anda -> _**New**_ ->  _**Package**_ -> Lalu tuliskan nama packagenya.

6. Langkah selanjutnya, buka link [Corona API](https://api.covid19api.com/summary) dan kita copy seluruh isinya. Setelah dicopy, lalu kita klik kanan pada package _**model**_ -> _**Generate POJO from JSON**_ -> Setelah terbuka dialog _**RoboPOJO**_, kita paste apa yang kita copy dan pastikan hasilnya hanya ada 1 baris. Setelah itu, ubah _**Root object name**_ menjadi _**CoronaResponse**_, pilih _**Language**_ menjadi _**Java**_, _**Framework**_ menjadi _**GSON**_, checklist semua yang berada di bawahnya, dan klik _**Generate**_. Setelah di _**Generate**_, di dalam package _**model**_ akan ada 3 java class dengan nama _**CoronaResponse**_, _**CountriesItem**_, dan _**Global**_.

![RoboPOJO2](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/robopojo2.png)

7. Setelah selesai dengan _**RoboPOJOGenerator**_, kita akan membuat 1 interface dengan nama _**CoronaAPI**_ dan 1 class dengan nama _**CoronaService**_ di dalam package _**service**_. Untuk membuat interface hampir sama dengan membuat class, perbedaannya adalah dengan memilih interface saat memasukkan namanya.

![Interface](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/interface.png)

8. Setelah itu sesuaikan kodingan yang berada di interface dan kelas tersebut seperti berikut ini,
    - `CoronaAPI.java`
        ```java
        String URL_BASE = "https://api.covid19api.com/";

        @GET("summary")
        Call<CoronaResponse> getCorona();
        ```
        - `URL_BASE` digunakan untuk menyimpan base dari url yang kita gunakan tadi.
        - Dikarenakan kita hanya mengambil data dan menggunakannya di dalam project kita, kita menggunakan query `@GET` dengan isi sisa dari url yang kita gunakan tadi.
    - `CoronaService.java`
        ```java
        public class CoronaService {
            private Retrofit retrofit = null;

            public CoronaAPI getAPI(){
                if (retrofit == null){
                    retrofit = new Retrofit
                            .Builder()
                            .baseUrl(CoronaAPI.URL_BASE)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                }
                return retrofit.create(CoronaAPI.class);
            }

            public void getCountries(final CoronaListener<List<CountriesItem>> listener){
                getAPI().getCorona().enqueue(new Callback<CoronaResponse>() {
                    @Override
                    public void onResponse(Call<CoronaResponse> call, Response<CoronaResponse> response) {
                        CoronaResponse data = response.body();

                        if (data != null && data.getGlobal() != null ){
                            listener.onSuccess(data.getCountries());
                        }
                    }

                    @Override
                    public void onFailure(Call<CoronaResponse> call, Throwable t) {
                        listener.onFailed(t.getMessage());
                    }
                });
            }
        }
        ```
9. Selanjutnya kita akan membuat 1 interface lagi di dalam package utama dengan nama `CoronaListener` **(Bukan di dalam package model maupun service)** dan sesuaikan isinya menjadi seperti ini.
    ```java
    public interface CoronaListener<T> {
        void onSuccess(T items);
        void onFailed(String msg);
    }
    ```
    Interface ini berfungsi untuk menghubungkan antara `MainActivity.java` dengan `CoronaService.java`.
10. Jika kita lihat kembali fungsi dari interface _**CoronaListener.java**_, selanjutnya kita akan masuk ke dalam `MainActivity.java` dan sesuaikan kodingannya menjadi seperti berikut ini,
    ```java
    public class MainActivity extends AppCompatActivity {
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            new CoronaService().getCountries(CountryListener);
        }

        CoronaListener<List<CountriesItem>> CountryListener = new CoronaListener<List<CountriesItem>>() {
            @Override
            public void onSuccess(List<CountriesItem> items) {
                for(int i = 0; i < items.size(); i++){
                    Log.d("Hasil : NAMA KOTA -> ", items.get(i).getCountry());
                    Log.d("Hasil : CONFIRMED -> ", String.valueOf(items.get(i).getTotalConfirmed()));
                }
            }

            @Override
            public void onFailed(String msg) {
                Log.d("ISI ERROR", msg);
            }
        };
    }
    ```
    - `getCountry()` adalah salah satu method yang ada di dalam kelas `CountriesItem.java`, anda dapat mengeluarkan data lain dengan melihat nama method yang ada di kelas tersebut.
    - Anda dapat mengembangkan kembali project ini dengan mengintegrasikan project ini kengan materi RecyclerView maupun Fragment.
    - Untuk melihat apakah project berjalan dengan sempurna adalah dengan melihat pada bagian bawah klik _**Logcat**_ -> Ubah _**Verbose**_ menjadi _**Debug**_ dan cari dengan keyword `NAMA KOTA :` seperti dibawah ini.

    ![Logcat](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/logcat.png)