### NestedView
Nah, kemarin kita sudah belajar banyak tipe-tipe View dan ViewGroup. Seperti yang dijelaskan di pertemuan sebelumnya, 
>ViewGroup adalah sebuah obyek yang mewadahi obyek-obyek View dan ViewGroup itu sendiri sehingga membentuk satu kesatuan tampilan aplikasi yang utuh.

Jadi, kita dapat menempatkan ViewGroup di dalam sebuah viewgroup itu sendiri. Jenis ini dapat kita namakan sebagai NestedView atau view bertingkat. Untuk komponen View dan ViewGroup dapat digambarkan melalui diagram berikut ini :

![Diagram](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/hierarki.png)

Agar lebih paham dalam penggunaan NestedView, anda dapat melihatnya di contoh kodingan berikut ini :
```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android" //Ini ViewGroup luar
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical" >

    <TextView android:id="@+id/text" //Ini View Luar
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="This is TextView" />

    <Button android:id="@+id/button" //Ini View Luar
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="This is Button" />

    <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android" //Ini ViewGroup dalam
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="horizontal">

        <TextView android:id="@+id/text2" //Ini View dalam
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="This is TextView" />

    </LinearLayout>
</LinearLayout>
```

### Integrasi Java dengan View
Sebelum memahami tentang integrasi java dan view maka kita perlu membuat sebuah project android.

**Membuat Project Android**
1.	Pada jendela Welcome to Android Studio, klik Start a new Android Studio project. Jika sudah membuka project, pilih File > New > New Project.
2.	Di jendela Select a Project Template, pilih Empty Activity lalu klik Next.
3.	Di jendela Configure your project, lakukan langkah-langkah berikut:
    -	Masukkan "My First App" dalam kolom Name.
    -	Masukkan "com.example.ITC" pada kolom Package name.
    -	Jika ingin menempatkan project di folder lain, ubah lokasi Save-nya.
    -	Pilih Java dari menu drop-down Language.
    -	Pilih versi terendah Android yang akan didukung aplikasi Anda di kolom Minimum SDK (Recommend, sdk 20).
    -	Jika aplikasi Anda memerlukan dukungan library lama, tandai kotak centang Use legacy android.support libraries.
    -	Biarkan opsi lain sebagaimana adanya.
4.	Klik Finish.

**Integrasi Java dan View**

Integrasi java dan view diperlukan agar tampilan view pada aplikasi dapat dihubungkan dengan java sehingga kita dapat mengatur view melalui java. 

Untuk mengintegrasi java dan view pertama kita perlu menghubungkan java dengan viewgroupnya dengan cara :

XML File
```
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <TextView
        android:id="@+id/textMain"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Hello World!" />

</LinearLayout>
```
Java Class
```
package com.example.myapplication;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);//view.xml yang ingin dihubungkan.
    }
}
```
Setelah viewgroup terhubung sekarang kita hanya perlu menghubungkan view dengan java. Dengan cara ```findViewById```.
```
package com.example.myapplication;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView favText;//mendeklarasikan TextView.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);//view.xml yang ingin dihubungkan.
        favText = findViewById(R.id.textMain);//menyimpan id view Textview.
    }
}
```

### Proyek Membuat Kalkulator Sederhana
Agar lebih paham, kita akan membuat kalkulator sederhana yang hanya dapat menambahkan 2 buah integer. Berikut cara pembuatan kalkulator sederhana.

1. Buat Project Baru Empty Activity.
2. Ubah activity_main.xml menjadi seperti berikut : 

```
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity"
    android:padding="14dp">

    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Kalkulator Sederhana"
        android:gravity="center"
        android:textColor="#000000"
        android:textSize="28sp"
        android:layout_marginBottom="8dp"/>

    <EditText
        android:id="@+id/textInput1"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:hint="0"
        android:textColor="#000000" />

    <EditText
        android:id="@+id/textInput2"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:hint="0"
        android:textColor="#000000" />

    <LinearLayout
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:orientation="horizontal">

        <Button
            android:id="@+id/btnCalculate"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Calculate"
            android:textColor="#000000"
            android:layout_marginRight="20dp"
            android:layout_marginHorizontal="15dp"/>

        <TextView
            android:id="@+id/textCalculation"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="0 + 0 = "
            android:textColor="#000000"
            android:textSize="20sp" />

        <TextView
            android:id="@+id/textMainHasil"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="0"
            android:textColor="#000000"
            android:textSize="20sp" />
    </LinearLayout>
</LinearLayout>
```
3. Langkah selanjutnya yaitu menyambungkan view dari file xml ke java dengan mengubah file MainActivity menjadi : 

```
package com.example.myapplication;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textHasil, textCalculate;
    EditText editValue1, editValue2;
    Button btnCalculation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textCalculate = findViewById(R.id.textCalculation);
        textHasil = findViewById(R.id.textMainHasil);
        btnCalculation = findViewById(R.id.btnCalculate);
        editValue1 = findViewById(R.id.textInput1);
        editValue2 = findViewById(R.id.textInput2);
    }
}
```

4. Setelah Menyambungkan view yang ada ke file javanya, sekarang kita akan membuat code java untuk tombol dan logika dari kalkulator sederhana. Untuk membuat codenya silahkan ikuti code dibawah.

```
package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView textHasil, textCalculate;
    EditText editValue1, editValue2;
    Button btnCalculation;
    int value1, value2, valueHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textCalculate = findViewById(R.id.textCalculation);
        textHasil = findViewById(R.id.textMainHasil);
        btnCalculation = findViewById(R.id.btnCalculate);
        editValue1 = findViewById(R.id.textInput1);
        editValue2 = findViewById(R.id.textInput2);
        btnCalculation.setOnClickListener(new View.OnClickListener() {//Membuat Fungsi Tombol
            @Override
            public void onClick(View view) {//Pada saat klik
                try {
                    value1 = Integer.parseInt(editValue1.getText().toString());//Mengubah Input data String menjadi int
                    value2 = Integer.parseInt(editValue2.getText().toString());//Mengubah Input data String menjadi int
                    valueHasil = value1 + value2;
                    textCalculate.setText(value1 + " + " + value2 + " = ");
                    textHasil.setText(String.valueOf(valueHasil));//Mengubah nilai ke string dan menampilkanya
                    Toast.makeText(view.getContext(), value1 + " + " + value2 + " = " + valueHasil, Toast.LENGTH_SHORT).show();
                    // Ketika menekan tombol calculate akan menampilkan tulisan hasil penjumlahan yang ada di dalam toast
                } catch (Exception e) {//user menginput selain int maka catch akan mulai
                    Toast.makeText(view.getContext(), "Hanya Dapat Input Angka!", Toast.LENGTH_LONG).show();
                    // Ketika menekan tombol calculate akan menampilkan error yang ada di dalam toast
                }
            }
        });
    }
}

```
5. Jika semua berjalan dengan benar maka tampilan aplikasi saat dijalankan akan seperti dibawah.

![Contoh Hasil](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/hasil1.jpeg)
> Tampilan akan menjadi seperti itu apabila proses dijalankan dengan benar.

![Contoh Hasil](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/hasil2.jpeg)
> Tampilan akan menjadi seperti itu apabila ada inputan selain angka.

 - Pada saat tombol calculate di klik maka aplikasi akan menampilkan hasil pertambahan kedua nilai.
 - Jika user mengisi input selain tipe integer maka akan muncul text exception.