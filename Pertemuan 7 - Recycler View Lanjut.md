# Recycler View Lanjut
### Pendahuluan
Pada pertemuan sebelumnya kita sudah berhasil membuat dan mempelajari materi yang lumayan sulit yaitu RecyclerView. Dari materi dasar tersebut dapat kita kembangkan lagi dengan mengintegrasikan materi RecyclerView dengan materi-materi sebelumnya. Untuk materi hari ini, kita akan mempelajari bagaimana cara mengintegrasikan RecyclerView dengan Intent agar kita dapat menampilkan dan sekaligus memberikan aksi pada aplikasi kita.

### Integrasi RecyclerView dengan Intent
Untuk mengembangkan materi ini, kita dapat menggunakan project sebelumnya untuk dilanjutkan pada pertemuan kali ini. Agar lebih mudah dipahami, mari kita langsung menuju projectnya.
1. Bukalah kembali project yang telah kita buat sebelumnya dengan nama _**MyPlay**_.
2. Setelah project dibuka, buatlah activity baru dengan nama _**DetailActivity**_ dan sesuaikan kodingan yang berada pada layout _**activity_detail.xml**_ menjadi seperti berikut,
    ```java
    <?xml version="1.0" encoding="utf-8"?>
    <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        tools:context=".DetailActivity">

        <androidx.cardview.widget.CardView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:cardCornerRadius="20dp"
            android:layout_margin="10dp">
            <RelativeLayout
                android:layout_width="match_parent"
                android:layout_height="50dp"
                android:orientation="vertical"
                android:background="@color/colorPrimary">
                <ImageView
                    android:id="@+id/iv_back"
                    android:layout_width="45dp"
                    android:layout_height="45dp"
                    android:src="@drawable/back"
                    android:layout_alignParentLeft="true"
                    android:layout_marginLeft="15dp"
                    android:layout_marginTop="2dp"/>
            </RelativeLayout>
        </androidx.cardview.widget.CardView>
        <ImageView
            android:id="@+id/iv_photo"
            android:layout_width="200dp"
            android:layout_height="200dp"
            tools:src="@drawable/ic_launcher_background"
            android:layout_marginTop="50dp"
            android:layout_gravity="center"/>

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical"
            android:padding="25dp">
            <TextView
                android:id="@+id/tv_name"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:text="Instagram"
                android:textSize="42sp"
                android:gravity="center"
                android:fontFamily="cursive"/>
            <TextView
                android:id="@+id/tv_detail"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:text="Ini Detail"
                android:gravity="center"
                android:fontFamily="casual"
                android:layout_marginTop="10dp"
                android:textSize="20sp"/>
        </LinearLayout>
    </LinearLayout>
    ```
    Pada bagian tersebut `android:src="@drawable/back"` akan berwarna merah atau error. Hal itu dikarenakan kita belum menambahkan sebuah vector pada project kita. Cara menambahkannya dapat mengikuti step berikut ini,
    - Klik kanan pada direktori _**drawable**_ pada bagian _**res**_ -> _**New**_ -> _**Vector Asset**_.
    - Setelah itu, klik pada bagian _**Clip Art**_ lalu cari gambar dengan kata kunci _**arrow back**_ dan ubah namanya menjadi _**back**_.
    - Ketika sudah mengganti namanya, lalu klik _**Next**_ dan _**Finish**_ untuk menyelesaikan proses tersebut.
    - Selain memilih gambar dan mengganti nama, kita juga dapat mengatur ukuran, warna, dan opacity pada dialog tersebut.
    
    ![Vector Asset](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/vector_asset.png) 
3. Setelah melakukan perubahan di bagian layout _**activity_detail.xml**_, kini kita masuk ke dalam _**PlaystoreAdapter.java**_ dan melakukan sedikit perubahan.
    - Tambahkan deklarasi `Context context;` pada bagian awal kelas dan constructor seperti dibawah ini,
        ```java
        public class PlaystoreAdapter extends RecyclerView.Adapter<PlaystoreAdapter.ViewHolder> {
            private ArrayList<Playstore> playStore;
            private Context context;

            public PlaystoreAdapter(ArrayList<Playstore> playStore, Context context) {
                this.playStore = playStore;
                this.context = context;
            }
            ...
        }
        ``` 
    - Setelah itu, karena kita akan nama, detail, dan gambar dari setiap aplikasi pada tampilan layar yang baru, kita membutuhkan sebuah Intent untuk dapat pindah dan juga sekalian mengirimkan datanya. Kodingannya akan diletakkan pada bagian method onBindViewHolder() menjadi seperti berikut ini,
        ```java
        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
            ...

            holder.cvCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtra("name", playstore.getsName());
                    intent.putExtra("detail", playstore.getsDetail());
                    intent.putExtra("photo", playstore.getsPhoto());
                    context.startActivity(intent);
                }
            });
        }
        ``` 
4. Setelah selesai dengan _**PlaystoreAdapter.java**_, kita lanjut ke _**DetailActivity.java**_ dan sesuaikan kodingan menjadi berikut ini,
    ```java
    public class DetailActivity extends AppCompatActivity {
        TextView tvName, tvDetail;
        ImageView ivPhoto, ivBack;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_detail);

            tvName = findViewById(R.id.tv_name);
            tvDetail = findViewById(R.id.tv_detail);
            ivPhoto = findViewById(R.id.iv_photo);
            ivBack = findViewById(R.id.iv_back);

            tvName.setText(getIntent().getStringExtra("name"));
            tvDetail.setText(getIntent().getStringExtra("detail"));

            Glide.with(getApplicationContext())
                    .load(getIntent().getStringExtra("photo"))
                    .into(ivPhoto);

            ivBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(DetailActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish(); // Fungsi penempatan finish() disini agar ketika kita tidak kembali ke laman DetailActivity setelah pindah ke MainActivity
                }
            });
        }
    }
    ```
5. Langkah terakhir, kita melakukan sedikit perubahan pada bagian _**MainActivity**_ dan method showRecyclerView() menjadi seperti ini,
    ```java
    private void showRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        PlaystoreAdapter listHeroAdapter = new PlaystoreAdapter(playstore,this);
        recyclerView.setAdapter(listHeroAdapter);
    }
    ```
6. Sampai di tahap ini semua kodingan sudah selesai dan dapat dijalankan.

Anda dapat membaca lebih lanjut mengenai Recycler View pada tautan berikut:
- [Recycler View](https://developer.android.com/reference/android/support/v7/widget/RecyclerView.html)